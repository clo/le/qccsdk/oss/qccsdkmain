/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
*/
/** System Generated File 
  * Cann't Change Manually */ 

#include "nt_devcfg_def.h" 
#include "nt_devcfg_types.h" 

#ifndef DAL_CONFIG_IMAGE_APPS 
#define DAL_CONFIG_IMAGE_APPS 
#endif 

 const void * DALPROP_StructPtrs_devcfg_byte_seq_xml[] =  {
	 NULL 
 };
 const uint32 DALPROP_PropBin_devcfg_byte_seq_xml[] = {

			0x00000040, 0x00000020, 0x00000020, 0x00000020, 0x00000020, 
			0x00000001, 0x04000001, 0x00000020, 0x08000001, 0x03020109, 
			0x07060504, 0x00030908, 0x08000002, 0xff0b5a06, 0x8158ea86, 
			0xff00ff00 };
