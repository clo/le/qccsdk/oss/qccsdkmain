/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
*/

#ifndef _NEUTRINO_REG_H_
#define _NEUTRINO_REG_H_

#include "Neutrino_hwiobase.h"
#include "Neutrino_seq_hwioreg.h"
#include "seq_hwio.h"

#endif /* _NEUTRINO_REG_H_ */
