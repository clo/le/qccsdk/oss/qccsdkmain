/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
*/
/** System Generated File 
  * Cann't Change Manually */ 

#include "nt_devcfg_types.h" 
#include "nt_devcfg_def.h" 
#include <stddef.h> 

extern const void * DALPROP_StructPtrs_devcfg_byte_seq_xml[];

extern const uint32 DALPROP_PropBin_devcfg_byte_seq_xml[];


const DALProps DALPROP_PropsInfo_devcfg_byte_seq_xml = {( const byte*)DALPROP_PropBin_devcfg_byte_seq_xml, DALPROP_StructPtrs_devcfg_byte_seq_xml , 0, NULL};
