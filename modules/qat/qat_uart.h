/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */
#include "qapi_uart.h"

#ifndef __QAT_UART_H__
#define __QAT_UART_H__

#define UART_HTC_INSTANCE QAPI_UART_INST_0	

void Initialize_Uart(void);

#endif


