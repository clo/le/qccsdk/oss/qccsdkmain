#===============================================================================
# Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
# SPDX-License-Identifier: BSD-3-Clause-Clear
#===============================================================================

"""
Provides the class to hold authentication data.
"""

class AuthParams(object):
    """Used to hold the authentication parameters to be passed to features for
    authentication.
    """

    def __init__(self):
        """Initializations if any.
        """
        pass
