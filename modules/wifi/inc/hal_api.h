/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
*/


/*
 * @brief top level HAL api header file
 */

#ifndef _HAL_API_H_
#define _HAL_API_H_

#include "hal_api_ctl.h"
#include "hal_api_sys.h"
#include "hal_api_bcn.h"

#endif // _HAL_API_H_
