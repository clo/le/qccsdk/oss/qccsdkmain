/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
*/

#ifndef __GPIO_DEMO_H__
#define __GPIO_DEMO_H__


void gpio_shell_init(void);

#endif
