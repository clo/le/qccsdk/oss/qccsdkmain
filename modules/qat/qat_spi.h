/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#ifndef __QAT_SPI_H__
#define __QAT_SPI_H__


int QAT_SPI_Output(uint32_t Length, const char *Buffer);


#endif


