/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

/**
 *    @file
 *          Platform-specific configuration overrides for the CHIP BLE
 *          Layer on Fermion platforms
 *
 */

#pragma once

// ==================== Platform Adaptations ====================


// ========== Platform-specific Configuration Overrides =========

/* none so far */
