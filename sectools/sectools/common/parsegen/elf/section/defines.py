#===============================================================================
# Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
# SPDX-License-Identifier: BSD-3-Clause-Clear
#===============================================================================

SN_STRING  = 'Name'
SF_STRING  = 'Flags'
SA_STRING  = 'Addr'
SO_STRING  = 'Offset'
SS_STRING  = 'Size'
SL_STRING  = 'Link'
SI_STRING  = 'Info'
SAL_STRING = 'Align'
SE_STRING  = 'EntSz'

SHN_UNDEF = 0

