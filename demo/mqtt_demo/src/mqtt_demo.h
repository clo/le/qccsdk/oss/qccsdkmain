/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#ifndef MQTT_DEMO_H
#define MQTT_DEMO_H

/**
 * @brief The AP SSID.
 * 
 * #define WLAN_AP_SSID         ".....insert here...."
 */
#define WLAN_AP_SSID "D-Link_DIR-816"

#endif
