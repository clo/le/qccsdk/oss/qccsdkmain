/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
*/


#ifndef CORE_SYSTEM_INC_EXT_RTC_INTR_H_
#define CORE_SYSTEM_INC_EXT_RTC_INTR_H_
#define        NVIC_ISER0_EXT_INT_ENABLE    0x29
void ext_irq_handler(void);




#endif /* CORE_SYSTEM_INC_EXT_RTC_INTR_H_ */
