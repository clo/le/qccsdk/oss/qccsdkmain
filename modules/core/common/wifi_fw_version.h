/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
*/

/*========================================================================
*
* @file wifi_fw_version.h
* @brief wlan firmware build defines
*========================================================================*/


#ifndef WIFI_FW_VERSION_BUILD
#define WIFI_FW_VERSION_BUILD
#define WIFI_FW_VER_MAJOR 0
#define WIFI_FW_VER_MINOR 1
#define WIFI_FW_VER_COUNT 1
#define WIFI_FW_VARIANT_NAME "WiFi_Firmware_qcc730"
#endif

