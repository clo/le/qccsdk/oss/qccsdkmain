/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
*/

#ifndef _FERMION_REG_H_
#define _FERMION_REG_H_

#include "Fermion_hwiobase.h"
#include "Fermion_seq_hwioreg.h"
#include "seq_hwio.h"

#endif /* _FERMION_REG_H_ */
