#===============================================================================
# Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
# SPDX-License-Identifier: BSD-3-Clause-Clear
#===============================================================================

ENV_SIGNING_SERVER_HOST_TAG = 'SECTOOLS_SIGNER_HOST'
ENV_SIGNING_SERVER_PORT_TAG = 'SECTOOLS_SIGNER_PORT'
