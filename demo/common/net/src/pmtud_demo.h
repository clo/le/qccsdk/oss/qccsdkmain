/*
* Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
* SPDX-License-Identifier: BSD-3-Clause-Clear
*/

/*-------------------------------------------------------------------------
 * Include Files
 *-----------------------------------------------------------------------*/
#include "qapi_status.h"
#include "qcli_api.h"
#include "qapi_console.h"
/*-------------------------------------------------------------------------
 * Function Definitions
 *-----------------------------------------------------------------------*/
qapi_Status_t pmtud_demo(uint32_t Parameter_Count, QAPI_Console_Parameter_t *Parameter_List);


