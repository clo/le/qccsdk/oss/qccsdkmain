#===============================================================================
# Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
# SPDX-License-Identifier: BSD-3-Clause-Clear
#===============================================================================

#------------------------------------------------------------------------------
# ELF HEADER - Version (e_version)
#------------------------------------------------------------------------------
EV_NONE         = 0
EV_CURRENT      = 1

EV_STRING       = 'Version'
EV_DESCRIPTION = \
{
    EV_NONE     : '0x0',
    EV_CURRENT  : '0x1',
}
