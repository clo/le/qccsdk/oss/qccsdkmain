#===============================================================================
# Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
# SPDX-License-Identifier: BSD-3-Clause-Clear
#===============================================================================

PAD_BYTE_1 = b'\xff'
PAD_BYTE_0 = b'\x00'
