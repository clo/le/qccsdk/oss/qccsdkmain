/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
*/

#ifndef SBL_VERSION_BUILD
#define SBL_VERSION_BUILD
#define SBL_VER_MAJOR 0
#define SBL_VER_MINOR 1
#define SBL_VER_COUNT 33
#endif
