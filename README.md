#Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
#SPDX-License-Identifier: BSD-3-Clause-Clear

## Introduction
The Qualcomm QCC730 Software Development Kit (SDK) is designed for the QCC730 chips, which are IoT MCU chips capable of operating at ultra-low power while providing Wi-Fi connectivity and a variety of peripheral interfaces. This SDK includes a Wi-Fi library, TCP/IP stack, peripheral drivers, and corresponding APIs. Additionally, it offers tools for programming the image and a VSCode-based extension plugin for development.

## Getting Started
To begin using the QCC730 SDK:

1. Visit Qualcomm’s website: www.qualcomm.com
2. Search for “QCC730”.
3. Follow the instructions to obtain the document 80-Y8730-1 (AA) QCC730 Development Kit Quick Start Guide.
4. Follow the guide to start your development work.