/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
*/


#ifndef __QCCSDK_CONSOLE_H__
#define __QCCSDK_CONSOLE_H__

void qccsdk_console_init (void);

#endif

