/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
*/
#include "portmacro.h"
#include "FreeRTOSConfig.h"

#define assert(a) configASSERT(a)
