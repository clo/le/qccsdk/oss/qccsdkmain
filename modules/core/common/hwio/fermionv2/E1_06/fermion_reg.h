/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
*/

#ifndef FERMION_PHYREG_H
#define FERMION_PHYREG_H
#include "Fermion_hwiobase.h"
#include "Fermion_seq_hwioreg.h"
//#include "rfa_dig_seq_hwiobase.h"
//#include "wl_tpc_seq_hwioreg.h"
//#include "pmu_seq_hwioreg.h"
//#include "wl_txfe_seq_hwioreg.h"
//#include "wl_txbb_seq_hwioreg.h"
//#include "rbist_rx_seq_hwioreg.h"
//#include "wl_rxfe_seq_hwioreg.h"
//#include "wl_mc_seq_hwioreg.h"
//#include "wl_rxbb_seq_hwioreg.h"
//#include "drm_reg_seq_hwioreg.h"
//#include "bbpll_seq_hwioreg.h"
//#include "wl_synth_lo_seq_hwioreg.h"
//#include "wl_synth_clbs_seq_hwioreg.h"
//#include "wl_adc_seq_hwioreg.h"
//#include "clkgen_seq_hwioreg.h"
//#include "wl_synth_ac_seq_hwioreg.h"
//#include "xo_seq_hwioreg.h"
//#include "aon_seq_hwioreg.h"
//#include "rffe_m_seq_hwioreg.h"
//#include "wl_synth_bist_seq_hwioreg.h"
//#include "wl_synth_pc_seq_hwioreg.h"
//#include "wl_top_clkgen_seq_hwioreg.h"
//#include "wl_dac_seq_hwioreg.h"
//#include "mbias_seq_hwioreg.h"
//#include "wl_synth_bs_seq_hwioreg.h"
#include "seq_hwio.h"


#endif /*FERMION_PHYREG_H*/
