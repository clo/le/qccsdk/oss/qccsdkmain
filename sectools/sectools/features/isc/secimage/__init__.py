#===============================================================================
# Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
# SPDX-License-Identifier: BSD-3-Clause-Clear
#===============================================================================

__sectools_feature__ = "secimage"

# Name & version of the tool
SECIMAGE_TOOL_NAME = 'SecImage'
SECIMAGE_TOOL_VERSION = '5.74'

__version__ = SECIMAGE_TOOL_NAME + ' ' + SECIMAGE_TOOL_VERSION
