/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
*/

#ifndef __QAPI_WLAN_H__
#define __QAPI_WLAN_H__

/**
 * @file qapi_wlan.h
 *
 * @brief WLAN interface
 *
 * @details This file includes all other WLAN header files.
 */

#include "qapi_wlan_misc.h"
#include "qapi_wlan_param_group.h"
#include "qapi_wlan_base.h"
#include "qapi_wlan_coex.h"
#include "qapi_wlan_errors.h"

#endif // __QAPI_WLAN_H__

