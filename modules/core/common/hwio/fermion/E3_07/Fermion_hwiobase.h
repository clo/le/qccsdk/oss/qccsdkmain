/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
*/
///////////////////////////////////////////////////////////////////////////////////////////////
//
// wcss_seq_hwiobase.h : automatically generated by Autoseq  3.10 11/22/2022
// User Name:c_molone
//
// !! WARNING !!  DO NOT MANUALLY EDIT THIS FILE.
//
///////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WCSS_SEQ_BASE_H__
#define __WCSS_SEQ_BASE_H__

#ifdef SCALE_INCLUDES
	#include "HALhwio.h"
#else
	#include "msmhwio.h"
#endif


///////////////////////////////////////////////////////////////////////////////////////////////
// Instance Relative Offsets from Block wcss
///////////////////////////////////////////////////////////////////////////////////////////////

#define SEQ_WCSS_OTP_OFFSET                                          0x001a0000
#define SEQ_WCSS_TXP_OFFSET                                          0x02080400
#define SEQ_WCSS_TPE_OFFSET                                          0x02082000
#define SEQ_WCSS_DPU_OFFSET                                          0x02081800
#define SEQ_WCSS_ADU_OFFSET                                          0x02082800
#define SEQ_WCSS_BMU_OFFSET                                          0x02080000
#define SEQ_WCSS_RXP_OFFSET                                          0x02080800
#define SEQ_WCSS_RPE_OFFSET                                          0x02082400
#define SEQ_WCSS_MCU_OFFSET                                          0x02080c00
#define SEQ_WCSS_MTU_OFFSET                                          0x02081400
#define SEQ_WCSS_DAHB_OFFSET                                         0x020c0000
#define SEQ_WCSS_CAHB_OFFSET                                         0x02082c00
#define SEQ_WCSS_FDAHB_OFFSET                                        0x020c0400
#define SEQ_WCSS_PMU_OFFSET                                          0x011aec00
#define SEQ_WCSS_UART_OFFSET                                         0x01233800
#define SEQ_WCSS_I2C_OFFSET                                          0x01233900
#define SEQ_WCSS_GPIO_OFFSET                                         0x01233a00
#define SEQ_WCSS_DWSPI_SLAVE_OFFSET                                  0x01233b00
#define SEQ_WCSS_QCSPI_SLAVE_OFFSET                                  0x01264000
#define SEQ_WCSS_CMEM_OFFSET                                         0x011a0400
#define SEQ_WCSS_CCU_OFFSET                                          0x0119a000
#define SEQ_WCSS_DXE_0_OFFSET                                        0x0119f000
#define SEQ_WCSS_CDAHB_OFFSET                                        0x011a0000
#define SEQ_WCSS_AGC_OFFSET                                          0x02013c00
#define SEQ_WCSS_BTCF_OFFSET                                         0x02014800
#define SEQ_WCSS_FFT_OFFSET                                          0x0200c000
#define SEQ_WCSS_MPI_OFFSET                                          0x02013400
#define SEQ_WCSS_PHYDBG_OFFSET                                       0x02004000
#define SEQ_WCSS_PHYINT_OFFSET                                       0x02015c00
#define SEQ_WCSS_PMI_OFFSET                                          0x02013800
#define SEQ_WCSS_RACTL_OFFSET                                        0x0200f000
#define SEQ_WCSS_RBAPB_OFFSET                                        0x02014400
#define SEQ_WCSS_RFIF_OFFSET                                         0x02015000
#define SEQ_WCSS_RXACLKCTRL_OFFSET                                   0x02016000
#define SEQ_WCSS_RXCLKCTRL_OFFSET                                    0x02016400
#define SEQ_WCSS_TACTL_OFFSET                                        0x02012400
#define SEQ_WCSS_TAQAM_OFFSET                                        0x02012c00
#define SEQ_WCSS_TBAPB_OFFSET                                        0x02012800
#define SEQ_WCSS_TDC_OFFSET                                          0x02014c00
#define SEQ_WCSS_TPC_OFFSET                                          0x02010000
#define SEQ_WCSS_TXCTL_OFFSET                                        0x0200b000
#define SEQ_WCSS_TXCLKCTRL_OFFSET                                    0x02013000
#define SEQ_WCSS_TXFIR_OFFSET                                        0x02018000
#define SEQ_WCSS_CAL_OFFSET                                          0x02008000
#define SEQ_WCSS_QTMR_AC_OFFSET                                      0x011a8000
#define SEQ_WCSS_QTMR_V1_T0_OFFSET                                   0x011a9000
#define SEQ_WCSS_QTMR_V1_T1_OFFSET                                   0x011aa000
#define SEQ_WCSS_QTMR_V1_T2_OFFSET                                   0x011ab000
#define SEQ_WCSS_QTMR_V1_T3_OFFSET                                   0x011ac000
#define SEQ_WCSS_QTMR_V1_T4_OFFSET                                   0x011ad000
#define SEQ_WCSS_CSCTI_OFFSET                                        0xe0042000
#define SEQ_WCSS_CXTMC_F32W4K_OFFSET                                 0xe0043004
#define SEQ_WCSS_QSPI_OFFSET                                         0x01190000
#define SEQ_WCSS_RRAM_CTRL_OFFSET                                    0x01192400
#define SEQ_WCSS_FCC_CSR_REG_OFFSET                                  0x01192000
#define SEQ_WCSS_CACHE_REGS_OFFSET                                   0x01180000
#define SEQ_WCSS_KDF_CSR_OFFSET                                      0x01233c00
#define SEQ_WCSS_LOCK_WRAPPER_L1_OFFSET                              0x01234800
#define SEQ_WCSS_PERISS_CRYPTO_CORE_OFFSET                           0x01234c00
#define SEQ_WCSS_ELP_PKA_AHB_OFFSET                                  0x01244c00
#define SEQ_WCSS_LOCK_WRAPPER_L2_OFFSET                              0x01248c00
#define SEQ_WCSS_PRNG_OFFSET                                         0x0124cc00
#define SEQ_WCSS_XPU2_OFFSET                                         0x0124d000
#define SEQ_WCSS_XPU2AHB_OFFSET                                      0x0125d000
#define SEQ_WCSS_CPR_WRAPPER_OFFSET                                  0x01260000
#define SEQ_WCSS_CXC_BMH_REG_OFFSET                                  0x02085000
#define SEQ_WCSS_CXC_LCMH_REG_OFFSET                                 0x02087000
#define SEQ_WCSS_CXC_MCIBASIC_REG_OFFSET                             0x02089000
#define SEQ_WCSS_CXC_LMH_REG_OFFSET                                  0x0208b000
#define SEQ_WCSS_CXC_SMH_REG_OFFSET                                  0x0208d000
#define SEQ_WCSS_CXC_PMH_REG_OFFSET                                  0x0208f000
#define SEQ_WCSS_WL_MC_OFFSET                                        0x02040000
#define SEQ_WCSS_MBIAS_OFFSET                                        0x02040200
#define SEQ_WCSS_XO_OFFSET                                           0x02040400
#define SEQ_WCSS_CLKGEN_CLBS_OFFSET                                  0x02040434
#define SEQ_WCSS_WL_SYNTH_BS_OFFSET                                  0x02040800
#define SEQ_WCSS_WL_SYNTH_CLBS_OFFSET                                0x02040840
#define SEQ_WCSS_WL_SYNTH_BIST_OFFSET                                0x0204087c
#define SEQ_WCSS_WL_SYNTH_PC_OFFSET                                  0x020408c0
#define SEQ_WCSS_WL_SYNTH_AC_OFFSET                                  0x02040900
#define SEQ_WCSS_WL_SYNTH_LO_OFFSET                                  0x02040940
#define SEQ_WCSS_BBPLL_OFFSET                                        0x02040a00
#define SEQ_WCSS_DRM_REG_OFFSET                                      0x02040b00
#define SEQ_WCSS_RBIST_TX_BAREBONE_OFFSET                            0x02041000
#define SEQ_WCSS_WL_DAC_OFFSET                                       0x02041100
#define SEQ_WCSS_WL_ADC_OFFSET                                       0x02041400
#define SEQ_WCSS_WL_RXFE_2G_OFFSET                                   0x02041600
#define SEQ_WCSS_WL_RXFE_5G_OFFSET                                   0x02041700
#define SEQ_WCSS_WL_TXFE_2G_OFFSET                                   0x02041800
#define SEQ_WCSS_WL_TXFE_5G_OFFSET                                   0x02041900
#define SEQ_WCSS_WL_RXBB_OFFSET                                      0x02042000
#define SEQ_WCSS_WL_TXBB_OFFSET                                      0x02042400
#define SEQ_WCSS_WL_TPC_OFFSET                                       0x02042800
#define SEQ_WCSS_RPMU_OFFSET                                         0x02043000


#endif

