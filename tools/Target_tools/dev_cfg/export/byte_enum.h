/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
*/
/** System Generated File
*  Don't Change Manually */
#ifndef CORE_COMMON_NT_DEVCFG_BYTE_SEQ_H_
#define CORE_COMMON_NT_DEVCFG_BYTE_SEQ_H_
typedef enum nt_devcfg_byte_seq_id_s 
{
byte_seq_one = 2,
byte_seq_two = 3,
}nt_devcfg_byte_seq_id_t; 
void* nt_devcfg_byte_seq_config(int enum_id);         // callback function for byte sequence value
#endif /* CORE_COMMON_NT_DEVCFG_BYTE_SEQ_H_ */
