/*===========================================================================

                    Crypto Engine Core API

GENERAL DESCRIPTION


EXTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
SPDX-License-Identifier: BSD-3-Clause-Clear
===========================================================================*/

/*===========================================================================
                      EDIT HISTORY FOR FILE
 
  $Header: //components/rel/core.ioe/1.0/v2/rom/release/api/security/crypto/private/hw/apps_proc/CeCL_Target.h#9 $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
10/29/15   yk      initial version
===========================================================================*/

/*===========================================================================
                     INCLUDE FILES FOR MODULE
===========================================================================*/

#include "HALhwio.h"
#include "Fermion_seq_hwioreg.h"
#include "Fermion_hwiobase.h"
#include "fermion_hw_reg.h"

//#include "syspm.h"
//#include "InterruptController.h"

/*===========================================================================
                 DEFINITIONS AND TYPE DECLARATIONS
===========================================================================*/

#define CECL_NPA_LPM_STRING      "blsppwr"
#define CECL_NPA_LPM_REQ_STATE   0x3//BLSP_PWR_ON

/*===========================================================================

                      PUBLIC DATA DECLARATIONS

===========================================================================*/

/* Refer to IPCAT/Quartz/Interrupts for below information */
#define CECL_IRQ_NUM                         19
#define CECL_IRQ_TRIGGER                     0x3//INTCF_TRIGGER_RISING
#define CECL_CE_CONFIG_VALUE                 0x3C0D
//#define CECL_GCC_CBCR                        GCC_GCC_QCC_AHB_CBCR_REG //??? PMU_TOP_AHB_CBCR? PMU_COMMON_AHB_CBCR? PMU_SIF_AHB_CBCR? PMU_WUR_AHB_CBCR?

#define CECL_CE_VERSION                      QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_VERSION_REG //QCC_CRYPTO_VERSION
#define CECL_CE_STATUS_SW_ERR                QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_STATUS_SW_ERR_MASK //HWIO_QCC_CRYPTO_STATUS_SW_ERR_BMSK 
#define CECL_CE_STATUS_ERR_INTR              QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_STATUS_ERR_INTR_MASK//HWIO_QCC_CRYPTO_STATUS_ERR_INTR_BMSK 
#define CECL_CE_CONFIG                       QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_CONFIG_REG //QCC_CRYPTO_CONFIG
#define CECL_CE_CORE_CONFIG                  QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_CORE_CFG_REG //QCC_CRYPTO_CORE_CFG
#define CECL_CE_STATUS                       QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_STATUS_REG//QCC_CRYPTO_STATUS
//#define CECL_CE_ENGINES_AVAIL                QCC_CRYPTO_ENGINES_AVAIL
#define CECL_CE_DATA_IN                      QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_DATA_IN_REG //QCC_CRYPTO_DATA_IN
#define CECL_CE_DATA_IN1                     QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_DATA_IN1_REG// QCC_CRYPTO_DATA_IN1
#define CECL_CE_DATA_IN2                     QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_DATA_IN2_REG//QCC_CRYPTO_DATA_IN2
#define CECL_CE_DATA_IN3                     QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_DATA_IN3_REG//QCC_CRYPTO_DATA_IN3
#define CECL_CE_DATA_OUT                     QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_DATA_OUT_REG//QCC_CRYPTO_DATA_OUT
#define CECL_CE_DATA_OUT1                    QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_DATA_OUT1_REG// QCC_CRYPTO_DATA_OUT1
#define CECL_CE_DATA_OUT2                    QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_DATA_OUT2_REG// QCC_CRYPTO_DATA_OUT2
#define CECL_CE_DATA_OUT3                    QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_DATA_OUT3_REG// QCC_CRYPTO_DATA_OUT3
#define CECL_CE_AUTH_IV0                     QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_AUTH_IV0_REG//QCC_CRYPTO_AUTH_IV0
#define CECL_CE_AUTH_IV1                     QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_AUTH_IV1_REG//QCC_CRYPTO_AUTH_IV1
#define CECL_CE_AUTH_IV2                     QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_AUTH_IV2_REG//QCC_CRYPTO_AUTH_IV2
#define CECL_CE_AUTH_IV3                     QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_AUTH_IV3_REG//QCC_CRYPTO_AUTH_IV3
#define CECL_CE_AUTH_IV4                     QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_AUTH_IV4_REG//QCC_CRYPTO_AUTH_IV4
#define CECL_CE_AUTH_IV5                     QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_AUTH_IV5_REG//QCC_CRYPTO_AUTH_IV5
#define CECL_CE_AUTH_IV6                     QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_AUTH_IV6_REG//QCC_CRYPTO_AUTH_IV6
#define CECL_CE_AUTH_IV7                     QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_AUTH_IV7_REG//QCC_CRYPTO_AUTH_IV7

#define CECL_CE_AUTH_BYTECNT0                QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_AUTH_BYTECNT0_REG//QCC_CRYPTO_AUTH_BYTECNT0 //???
#define CECL_CE_AUTH_BYTECNT1                QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_AUTH_BYTECNT1_REG//QCC_CRYPTO_AUTH_BYTECNT1
#define CECL_CE_AUTH_SEG_CFG                 QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_AUTH_SEG_CFG_REG //QCC_CRYPTO_AUTH_SEG_CFG
#define CECL_CE_GOPROC                       QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_GOPROC_REG//QCC_CRYPTO_GOPROC
#define CECL_CE_SEG_SIZE                     QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_SEG_SIZE_REG//QCC_CRYPTO_SEG_SIZE
#define CECL_CE_GOPROC_GO_BMSK               QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_GOPROC_GO_MASK//HWIO_QCC_CRYPTO_GOPROC_GO_BMSK
#define CECL_CE_AUTH_SEG_CFG_AUTH_ALG_SHFT           QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_AUTH_SEG_CFG_AUTH_ALG_OFFSET//HWIO_QCC_CRYPTO_AUTH_SEG_CFG_AUTH_ALG_SHFT
#define CECL_CE_AUTH_SEG_CFG_AUTH_SIZE_SHFT          QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_AUTH_SEG_CFG_AUTH_SIZE_OFFSET//HWIO_QCC_CRYPTO_AUTH_SEG_CFG_AUTH_SIZE_SHFT
//#define CECL_CE_AUTH_SEG_SIZE_AUTH_SIZE_SHFT         HWIO_QCC_CRYPTO_AUTH_SEG_SIZE_AUTH_SIZE_SHFT
#define CECL_CE_AUTH_SEG_SIZE                      QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_AUTH_SEG_SIZE_REG//QCC_CRYPTO_AUTH_SEG_SIZE
#define CECL_CE_AUTH_SEG_START                     QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_AUTH_SEG_START_REG//QCC_CRYPTO_AUTH_SEG_START
   

#define CECL_CE_AUTH_SEG_CFG_AUTH_KEY_SZ_SHFT     QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_AUTH_SEG_CFG_AUTH_KEY_SZ_OFFSET//HWIO_QCC_CRYPTO_AUTH_SEG_CFG_AUTH_KEY_SZ_SHFT
#define CECL_CE_AUTH_SEG_CFG_AUTH_MODE_SHFT       QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_AUTH_SEG_CFG_AUTH_MODE_OFFSET//HWIO_QCC_CRYPTO_AUTH_SEG_CFG_AUTH_MODE_SHFT
//#define CECL_CE_AUTH_SEG_CFG_AUTH_USE_HW_KEY_SHFT HWIO_QCC_CRYPTO_AUTH_SEG_CFG_USE_HW_KEY_AUTH_SHFT
#define CECL_CE_AUTH_SEG_CFG_AUTH_POS_SHFT        QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_AUTH_SEG_CFG_AUTH_POS_OFFSET//HWIO_QCC_CRYPTO_AUTH_SEG_CFG_AUTH_POS_SHFT
   
#define CECL_CE_AUTH_INFO_NONCE0                  QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_AUTH_INFO_NONCE0_REG//QCC_CRYPTO_AUTH_INFO_NONCE0
#define CECL_CE_AUTH_INFO_NONCE1                  QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_AUTH_INFO_NONCE1_REG//QCC_CRYPTO_AUTH_INFO_NONCE1
#define CECL_CE_AUTH_INFO_NONCE2                  QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_AUTH_INFO_NONCE2_REG//QCC_CRYPTO_AUTH_INFO_NONCE2
#define CECL_CE_AUTH_INFO_NONCE3                  QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_AUTH_INFO_NONCE3_REG//QCC_CRYPTO_AUTH_INFO_NONCE3

//#define CECL_CE_STATUS_CRYPTO_STATE_BMSK     HWIO_QCC_CRYPTO_STATUS_CRYPTO_STATE_BMSK
//#define CECL_CE_SEG_CFG_CLR_CNTXT_SHFT        HWIO_QCC_CRYPTO_SEG_CFG_CLR_CNTXT_SHFT
#define CECL_CE_STATUS_OPERATION_DONE_BMSK       QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_STATUS_OPERATION_DONE_MASK//HWIO_QCC_CRYPTO_STATUS_OPERATION_DONE_BMSK
#define CECL_CE_STATUS_DIN_RDY_BMSK              QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_STATUS_DIN_RDY_MASK//HWIO_QCC_CRYPTO_STATUS_DIN_RDY_BMSK
#define CECL_CE_STATUS_DIN_RDY_SHFT              QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_STATUS_DIN_RDY_OFFSET//HWIO_QCC_CRYPTO_STATUS_DIN_RDY_SHFT
#define CECL_CE_STATUS_DOUT_RDY_BMSK             QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_STATUS_DOUT_RDY_MASK//HWIO_QCC_CRYPTO_STATUS_DOUT_RDY_BMSK
#define CECL_CE_STATUS_DOUT_RDY_SHFT             QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_STATUS_DOUT_RDY_OFFSET// HWIO_QCC_CRYPTO_STATUS_DOUT_RDY_SHFT
//#define CECL_CE_AUTH_SEG_CFG_USE_HW_KEY_SHFT       HWIO_QCC_CRYPTO_AUTH_SEG_CFG_USE_HW_KEY_SHFT

#define CECL_CE_DATA_OUT_BUF_BASE QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_DATA_OUT_BUF_BASE_REG//QCC_CRYPTO_DATA_OUT_BUF_BASE
#define CECL_CE_DATA_OUT_BUF_LEN QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_DATA_OUT_BUF_LEN_REG//QCC_CRYPTO_DATA_OUT_BUF_LEN
#define CECL_CE_DATA_IN_BUF_BASE QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_DATA_IN_BUF_BASE_REG//QCC_CRYPTO_DATA_IN_BUF_BASE
#define CECL_CE_DATA_IN_BUF_LEN QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_DATA_IN_BUF_LEN_REG//QCC_CRYPTO_DATA_IN_BUF_LEN

#define CECL_CE_ENCR_SEG_CFG                      QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_ENCR_SEG_CFG_REG//QCC_CRYPTO_ENCR_SEG_CFG
#define CECL_CE_ENCR_CNTR0_IV0                    QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_ENCR_CNTR0_IV0_REG//QCC_CRYPTO_ENCR_CNTR0_IV0
#define CECL_CE_ENCR_CNTR1_IV1                    QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_ENCR_CNTR1_IV1_REG//QCC_CRYPTO_ENCR_CNTR1_IV1
#define CECL_CE_ENCR_CNTR2_IV2                    QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_ENCR_CNTR2_IV2_REG//QCC_CRYPTO_ENCR_CNTR2_IV2
#define CECL_CE_ENCR_CNTR3_IV3                    QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_ENCR_CNTR3_IV3_REG//QCC_CRYPTO_ENCR_CNTR3_IV3
#define CECL_CE_ENCR_CNTR_MASK3                   QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_ENCR_CNTR_MASK_REG//QCC_CRYPTO_ENCR_CNTR_MASK
#define CECL_CE_ENCR_CNTR_MASK0                   QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_ENCR_CNTR_MASK0_REG//QCC_CRYPTO_ENCR_CNTR_MASK0
#define CECL_CE_ENCR_CNTR_MASK1                   QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_ENCR_CNTR_MASK1_REG//QCC_CRYPTO_ENCR_CNTR_MASK1
#define CECL_CE_ENCR_CNTR_MASK2                   QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_ENCR_CNTR_MASK2_REG//QCC_CRYPTO_ENCR_CNTR_MASK2
#define CECL_CE_ENCR_CCM_INIT_CNTR0               QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_ENCR_CCM_INIT_CNTR0_REG//QCC_CRYPTO_ENCR_CCM_INIT_CNTR0
#define CECL_CE_ENCR_CCM_INIT_CNTR1               QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_ENCR_CCM_INIT_CNTR1_REG//QCC_CRYPTO_ENCR_CCM_INIT_CNTR1
#define CECL_CE_ENCR_CCM_INIT_CNTR2               QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_ENCR_CCM_INIT_CNTR2_REG//QCC_CRYPTO_ENCR_CCM_INIT_CNTR2
#define CECL_CE_ENCR_CCM_INIT_CNTR3               QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_ENCR_CCM_INIT_CNTR3_REG//QCC_CRYPTO_ENCR_CCM_INIT_CNTR3
#define CECL_CE_ENCR_SEG_CFG_ENCODE_SHFT          QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_ENCR_SEG_CFG_ENCODE_OFFSET//HWIO_QCC_CRYPTO_ENCR_SEG_CFG_ENCODE_SHFT
#define CECL_CE_ENCR_SEG_CFG_ENCR_MODE_SHFT       QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_ENCR_SEG_CFG_ENCR_MODE_OFFSET//HWIO_QCC_CRYPTO_ENCR_SEG_CFG_ENCR_MODE_SHFT
#define CECL_CE_ENCR_SEG_CFG_ENCR_ALG_SHFT        QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_ENCR_SEG_CFG_ENCR_ALG_OFFSET//HWIO_QCC_CRYPTO_ENCR_SEG_CFG_ENCR_ALG_SHFT
#define CECL_CE_ENCR_SEG_CFG_ENCR_KEY_SZ_SHFT     QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_ENCR_SEG_CFG_ENCR_KEY_SZ_OFFSET//HWIO_QCC_CRYPTO_ENCR_SEG_CFG_ENCR_KEY_SZ_SHFT
#define CECL_CE_ENCR_SEG_CFG_ENCR_MODE_BMSK       QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_ENCR_SEG_CFG_ENCR_MODE_MASK//HWIO_QCC_CRYPTO_ENCR_SEG_CFG_ENCR_MODE_BMSK
//#define CECL_CE_ENCR_SEG_CFG_ENCR_SIZE_SHFT       HWIO_QCC_CRYPTO_ENCR_SEG_CFG_ENCR_SIZE_SHFT
#define CECL_CE_ENCR_SEG_SIZE_ENCR_SIZE_SHFT      0//HWIO_QCC_CRYPTO_ENCR_SEG_SIZE_ENCR_SIZE_SHFT
#define CECL_CE_ENCR_SEG_SIZE                     QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_ENCR_SEG_SIZE_REG//QCC_CRYPTO_ENCR_SEG_SIZE
#define CECL_CE_ENCR_SEG_START                    QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_ENCR_SEG_START_REG//QCC_CRYPTO_ENCR_SEG_START
#define CECL_CE_SW_KEY0_0                         QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_SW_KEY0_0_REG//QCC_CRYPTO_SW_KEY0_0 
#define CECL_CE_SW_KEY0_1                         QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_SW_KEY0_1_REG//QCC_CRYPTO_SW_KEY0_1 
#define CECL_CE_SW_KEY0_2                         QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_SW_KEY0_2_REG//QCC_CRYPTO_SW_KEY0_2 
#define CECL_CE_SW_KEY0_3                         QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_SW_KEY0_3_REG//QCC_CRYPTO_SW_KEY0_3 
#define CECL_CE_SW_KEY0_4                         QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_SW_KEY0_4_REG//QCC_CRYPTO_SW_KEY0_4 
#define CECL_CE_SW_KEY0_5                         QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_SW_KEY0_5_REG//QCC_CRYPTO_SW_KEY0_5 
#define CECL_CE_SW_KEY0_6                         QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_SW_KEY0_6_REG//QCC_CRYPTO_SW_KEY0_6 
#define CECL_CE_SW_KEY0_7                         QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_SW_KEY0_7_REG//QCC_CRYPTO_SW_KEY0_7 

#define CECL_CE_SW_KEY1_0                         QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_SW_KEY1_0_REG//  QCC_CRYPTO_SW_KEY1_0 
#define CECL_CE_SW_KEY1_1                         QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_SW_KEY1_1_REG//  QCC_CRYPTO_SW_KEY1_1 
#define CECL_CE_SW_KEY1_2                         QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_SW_KEY1_2_REG//  QCC_CRYPTO_SW_KEY1_2 
#define CECL_CE_SW_KEY1_3                         QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_SW_KEY1_3_REG//  QCC_CRYPTO_SW_KEY1_3 

#define CECL_CE_SW_KEY2_0                         QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_SW_KEY2_0_REG//  QCC_CRYPTO_SW_KEY2_0 
#define CECL_CE_SW_KEY2_1                         QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_SW_KEY2_1_REG//  QCC_CRYPTO_SW_KEY2_1 
#define CECL_CE_SW_KEY2_2                         QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_SW_KEY2_2_REG//  QCC_CRYPTO_SW_KEY2_2 
#define CECL_CE_SW_KEY2_3                         QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_SW_KEY2_3_REG//  QCC_CRYPTO_SW_KEY2_3 

#define CECL_CE_SW_KEY3_0                         QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_SW_KEY3_0_REG//  QCC_CRYPTO_SW_KEY3_0 
#define CECL_CE_SW_KEY3_1                         QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_SW_KEY3_1_REG//  QCC_CRYPTO_SW_KEY3_1 
#define CECL_CE_SW_KEY3_2                         QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_SW_KEY3_2_REG//  QCC_CRYPTO_SW_KEY3_2 
#define CECL_CE_SW_KEY3_3                         QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_SW_KEY3_3_REG//  QCC_CRYPTO_SW_KEY3_3 

#define CECL_CE_KDF_KEY3_0                        QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_KDF_KEY3_0_REG//   QCC_CRYPTO_KDF_KEY3_0 
#define CECL_CE_KDF_KEY3_1                        QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_KDF_KEY3_1_REG//   QCC_CRYPTO_KDF_KEY3_1 
#define CECL_CE_KDF_KEY3_2                        QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_KDF_KEY3_2_REG//   QCC_CRYPTO_KDF_KEY3_2 
#define CECL_CE_KDF_KEY3_3                        QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_KDF_KEY3_3_REG//   QCC_CRYPTO_KDF_KEY3_3 
#define CECL_CE_PWR_CTRL                          QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_PWR_CTRL_REG//   QCC_CRYPTO_PWR_CTRL

#define CECL_CE_KEY_TABLE_CFG                     QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_KEY_TABLE_CFG_REG//   QCC_CRYPTO_KEY_TABLE_CFG
//KDF Lock registers
#define CECL_KDF_LOCK_LOCK_REQUEST_REG                QWLAN_LOCK_WRAPPER_R_LOCK_REQUEST_REG_REG//KDF_LOCK_LOCK_REQUEST_REG
#define CECL_KDF_LOCK_LOCK_REQUEST_LOCK_REQUEST_BMSK  QWLAN_LOCK_WRAPPER_R_LOCK_REQUEST_REG_LOCK_REQUEST_LOCK_REQUEST_MASK//HWIO_KDF_LOCK_LOCK_REQUEST_LOCK_REQUEST_BMSK
#define CECL_KDF_LOCK_LOCK_RELEASE_REG                QWLAN_LOCK_WRAPPER_R_LOCK_RELEASE_REG_REG//KDF_LOCK_LOCK_RELEASE_REG
#define CECL_KDF_LOCK_LOCK_RELEASE_LOCK_RELEASE_BMSK  QWLAN_LOCK_WRAPPER_R_LOCK_RELEASE_REG_LOCK_RELEASE_LOCK_RELEASE_MASK//HWIO_KDF_LOCK_LOCK_RELEASE_LOCK_RELEASE_BMSK
#define CECL_KDF_LOCK_LOCK_STATUS_REG                 QWLAN_LOCK_WRAPPER_R_LOCK_STATUS_REG_REG//KDF_LOCK_LOCK_STATUS_REG 
#define CECL_KDF_LOCK_LOCK_STATUS_LOCKED_STATUS       HWIO_KDF_LOCK_LOCK_STATUS_LOCKED_STATUS 
//#define CECL_KDF_LOCK_LOCK_STATUS_REG                 KDF_LOCK_LOCK_STATUS_REG
#define CECL_KDF_LOCK_LOCK_STATUS_LOCKED_VMID         HWIO_KDF_LOCK_LOCK_STATUS_LOCKED_VMID
#define CECL_KDF_LOCK_INTR_ENABLE_REG                 QWLAN_LOCK_WRAPPER_R_INTR_ENABLE_REG_REG//KDF_LOCK_INTR_ENABLE_REG 

//KDF registers
#define CECL_KDF_KDF_INTR_EN                          KDF_KDF_INTR_EN
#define CECL_KDF_KDF_INTERRUPT_M4_OP_DONE             HWIO_KDF_KDF_INTERRUPT_M4_OP_DONE
#define CECL_KDF_KDF_OP_CODE                          KDF_KDF_OP_CODE
#define CECL_KDF_KDF_PASSWORD_LOW                     KDF_KDF_PASSWORD_LOW
#define CECL_KDF_KDF_PASSWORD_HIGH                    KDF_KDF_PASSWORD_HIGH
#define CECL_KDF_KDF_INPUT_0                          KDF_KDF_INPUT_0
#define CECL_KDF_KDF_INPUT_1                          KDF_KDF_INPUT_1 
#define CECL_KDF_KDF_INPUT_2                          KDF_KDF_INPUT_2
#define CECL_KDF_KDF_INPUT_3                          KDF_KDF_INPUT_3
#define CECL_KDF_KDF_OP_GO                            KDF_KDF_OP_GO
#define CECL_KDF_KDF_OP_GO_SW_OP_GO_BMSK              HWIO_KDF_KDF_OP_GO_SW_OP_GO_BMSK
#define CECL_KDF_KDF_OP_STATUS                        KDF_KDF_OP_STATUS
#define CECL_KDF_KDF_OP_STATUS_SW_OP_STATUS_BMSK      HWIO_KDF_KDF_OP_STATUS_SW_OP_STATUS_BMSK

//#define CECL_SYS_SEC_CRYPTO_CFG()                 HWIO_OUT(QCC_CRYPTO_ENCR_SEG_CFG, seg_cfg_val);
#define CECL_SYS_SEC_CRYPTO_CFG()                 HWIO_OUT(QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_ENCR_SEG_CFG_REG, seg_cfg_val);
#define CECL_SYS_SEC_CRYPTO_GOPROC()              HWIO_OUT(QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_GOPROC_REG, QWLAN_PERISS_CRYPTO_CORE_R_CRYPTO_GOPROC_GO_MASK);
//#define CECL_SYS_SEC_CRYPTO_GOPROC()              HWIO_OUT(QCC_CRYPTO_GOPROC, HWIO_QCC_CRYPTO_GOPROC_GO_BMSK);
#define CECL_CE_CIPHER_AES_ALGO_VAL               0x2
#define CECL_CE_CIPHER_KEY_SIZE_AES128  0x0
#define CECL_CE_CIPHER_KEY_SIZE_AES256  0x2

#define CECL_AES_CCM_INIT_VECTOR_SIZE             8
#define CECL_AES_CCM_NONCE_VECTOR_SIZE            4
#define CECL_AES_CCM_CNTR_VECTOR_SIZE             4

#define CECL_CE_ENCR_SEG_CFG_ENCODE_DEC           0x0
#define CECL_CE_ENCR_SEG_CFG_ENCODE_ENC           0x1
/**
 * SHA algorithm type 
 */
#define CECL_CE_AUTH_SEG_CFG_AUTH_ALG_SHA        0x1
#define CECL_CE_AUTH_SEG_CFG_AUTH_SIZE_SHA1      0x0
#define CECL_CE_AUTH_SEG_CFG_AUTH_SIZE_SHA256    0x1

#define CECL_CE_AUTH_SEG_CFG_AUTH_ALG_AES         0x2
#define CECL_CE_AUTH_SEG_CFG_AUTH_KEY_SZ_128      0x0
#define CECL_CE_AUTH_SEG_CFG_AUTH_KEY_SZ_256      0x2
#define CECL_CE_AUTH_SEG_CFG_AUTH_MODE_CCM        0x0
#define CECL_CE_AUTH_SEG_CFG_AUTH_MODE_CBC_MAC    0x1
#define CECL_CE_AUTH_SEG_CFG_AUTH_MODE_HASH       0x0
#define CECL_CE_AUTH_SEG_CFG_AUTH_MODE_HMAC       0x1
#define CECL_CE_AUTH_SEG_CFG_AUTH_POS_BEFORE      0x0   //authentication before cipher
#define CECL_CE_AUTH_SEG_CFG_AUTH_POS_AFTER       0x1   //authentication after cipher

#define CECL_AES_CMAC_INIT_VECTOR_SIZE            4
#define CECL_AES_CCM_INIT_VECTOR_SIZE             8
#define CECL_AES_CCM_NONCE_VECTOR_SIZE            4
#define CECL_AES_CCM_CNTR_VECTOR_SIZE             4

#define CECL_SHA1_INIT_VECTOR_SIZE           5
#define CECL_SHA256_INIT_VECTOR_SIZE         8
					 
#define CECL_DISALGE_INTR() HWIO_OUTF(CECL_KDF_KDF_INTR_EN, INTERRUPT_M4_OP_DONE, 0)  
#define CECL_MY_VMID 0x1  

/* Set DATA_PATH_SEL_UP=0 which is just the POR value so no need of expnasion*/
#define CeClLprEnable()  

#define CeClLprDisable()
