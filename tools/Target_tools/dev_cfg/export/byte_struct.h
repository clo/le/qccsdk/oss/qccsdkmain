/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
*/
/** System Generated File
*  Don't Change Manually */
#ifndef CORE_DEV_CFG_INC_NT_DEVCFG_BYTE_SEQ_STRUCTURE_H_
#define CORE_DEV_CFG_INC_NT_DEVCFG_BYTE_SEQ_STRUCTURE_H_
#include "nt_devcfg_def.h" 
#include "nt_devcfg_types.h" 
void nt_devcfg_byte_seq_parse();
typedef struct nt_devcfg_byte_seq_structure_s
{
uint32 byte_seq_one;
uint32 byte_seq_two;
}nt_devcfg_byte_seq_structure_t;
#endif /* CORE_DEV_CFG_INC_NT_DEVCFG_BYTE_SEQ_STRUCTURE_H_ */
