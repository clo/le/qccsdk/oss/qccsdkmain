#===============================================================================
# Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
# SPDX-License-Identifier: BSD-3-Clause-Clear
#===============================================================================

PO_STRING           = 'Offset'
PV_STRING           = 'VAddr'
PP_STRING           = 'PAddr'
PF_STRING           = 'FileSz'
PM_STRING           = 'MemSz'
PA_STRING           = 'Align'
PFL_STRING          = 'Flags'
